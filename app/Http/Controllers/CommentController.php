<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CommentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_id)
    {
        $this->validate($request, [
            "body" => "required|max:5000"
        ]);

        $post = Post::findOrFail($post_id);

        $post->comments()->create([
            "user_id" => auth()->user()->id,
            "body" => $request->body
        ]);

        Session::flash('success', 'Comentário criada com sucesso');

        return redirect()->back();
    }
}

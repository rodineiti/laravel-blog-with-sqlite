<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ReplyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $comment_id)
    {
        $this->validate($request, [
            "body" => "required|max:5000"
        ]);

        $comment = Comment::findOrFail($comment_id);

        $comment->replies()->create([
            "user_id" => auth()->user()->id,
            "body" => $request->body
        ]);

        Session::flash('success', 'Resposta criada com sucesso');

        return redirect()->back();
    }
}

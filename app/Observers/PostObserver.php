<?php

namespace App\Observers;

use App\Post;
use App\Events\StorePost;

class PostObserver
{
    public function created(Post $model)
    {
        event(new StorePost($model));
    }

    public function deleting(Post $model)
    {
        event(new StorePost($model));
    }
}

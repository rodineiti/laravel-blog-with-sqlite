@extends('layouts.frontend')

@section('content')
    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-8">

            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
            @endif

            @include('partials.errors')

            <!-- Title -->
            <h1 class="mt-4">{{$post->id}} - {{$post->title}}</h1>

            <!-- Author -->
            <p class="lead">
                by
                <a href="{{route('posts.author', ["user_id" => $post->user_id])}}">{{$post->user->name}}</a>
            </p>

            <hr>

            <!-- Date/Time -->
            <p>Posted on {{$post->created_at}}</p>

            <hr>

            <!-- Preview Image -->
            @if($post->image)
                <img class="img-fluid rounded" src="{{$post->image_url}}" alt="Card image cap">
            @else
                <img class="img-fluid rounded" src="http://placehold.it/750x300" alt="Card image cap">
            @endif

            <hr>

            <!-- Post Content -->
            {!! $post->body !!}

            <hr>

            <nav>
                <ul class="pagination d-flex justify-content-between">
                    @if(isset($previousPost))
                        <li><a href="{{route('post.single', ['id' => $previousPost->id])}}">&lt;&lt; {{ $previousPost->id }} - {{ $previousPost->title }}</a></li>
                    @endif

                    @if(isset($nextPost))
                        <li><a href="{{route('post.single', ['id' => $nextPost->id])}}">{{ $nextPost->id }} - {{ $nextPost->title }} &gt;&gt;</a></li>
                    @endif
                </ul>
            </nav>

            <!-- Comments Form -->
            <div class="card my-4">
                <h5 class="card-header">Leave a Comment:</h5>
                <div class="card-body">                    
                    <form action="{{ route('comments.store', ['id' => $post->id]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="body"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Comment</button>
                    </form>
                </div>
            </div>

            <!-- Single Comment -->
            @foreach($post->comments()->orderBy("created_at", "DESC")->get() as $comment)
                <div class="media mb-4">
                    <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                    <div class="media-body">
                        <h5 class="mt-0">{{$comment->user->name}}</h5>
                        <p><i>{{$comment->created_at}}</i></p>
                        {!! $comment->body !!}

                        @foreach($comment->replies()->get() as $reply)
                            <div class="media mt-4">
                                <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                                <div class="media-body">
                                    <h5 class="mt-0">{{ $reply->user->name }}</h5>
                                    <p><i>{{$reply->created_at}}</i></p>
                                    {!! $reply->body !!}
                                </div>
                            </div>
                        @endforeach

                        <div class="card my-4">
                            <h5 class="card-header">Reply Comment:</h5>
                            <div class="card-body">                    
                                <form action="{{ route('replies.store', ['id' => $comment->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                    <div class="form-group">
                                        <textarea class="form-control" rows="3" name="body"></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Reply</button>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            @endforeach

        </div>

        <!-- Sidebar Widgets Column -->
        @include('partials.sidebar')

    </div>
    <!-- /.row -->
@endsection
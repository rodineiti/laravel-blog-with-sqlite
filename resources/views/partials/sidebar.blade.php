<div class="col-md-4">

    <!-- Search Widget -->
    <div class="card my-4">
        <h5 class="card-header">Search</h5>
        <div class="card-body">
            <div class="input-group">
                <form class="form-inline" action="{{route('welcome')}}" method="get">
                    <input type="text" class="form-control" placeholder="Search for..." name="term" />
                    <span class="input-group-append">
                        <button class="btn btn-secondary" type="submit">Go!</button>
                    </span>
                </form>
            </div>
        </div>
    </div>

    <!-- Categories Widget -->
    <div class="card my-4">
        <h5 class="card-header">Categories</h5>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-8">
                    <ul class="list-unstyled mb-0">
                        @foreach($categories as $category)
                            <li>
                                <a href="{{route('posts.category', ["category_id" => $category->id])}}">{{$category->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>